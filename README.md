TARDIS - Time and Relative Dimension in Snapshots
======

Snapshot management for Linux filesystems

Usage:

    tardis auto-snapshot [OPTIONS].. INTERVAL KEEP 
    tardis list [OPTIONS]
    tardis config [OPTIONS]

Options:

    --filesystem    -f  Select filesystem (ZFS, btrfs, etc)
    --read-only     -r  Create read-only snapshot (where applies)
    --no-action     -n  Take no action, show what would be done


Use Cases
---------
tardis can be used to create snapshots at custom-named intervals

For example, to create hourly snapshots, and keep up to 24 in the history.

    tardis auto-snapshot hourly 24

To create snapshots for a specific filesystem:

    tardis auto-snapshot --filesystem zfs daily 7
    
If an older snapshot is zero-sized, it will be deleted and the most recent snapshot will be kept.

To list all tardis-managed snapshots:

    tardis list

To view the configuration for tardis snapshots:

    tardis config


Filesystem Support
------------------
### ZFS
ZFS is fully supported, with configuration stored in the dataset properties.
Snapshots with the com.sun:auto-snapshot property unset or set to 'true' will be snapshotted.
Snapshots with com.sun:auto-snapshot:$interval=false will be excluded from that interval's snapshots.

### Btrfs
Configuration for btrfs subvolumes is based on the .btrfs-auto-snap directory in the root of the subvolume. 
Immediate subdirectories of .btrfs-auto-snapshot are the configured intervals for that subvolume.  
If an interval directory exists and is writable, auto-snapshot:$interval is enabled.  
If the subdirectory is disabled, so is auto-snapshot:$interval.  
If auto-snapshot is enabled for a subvolume, but the interval to be snapshotted is not present, it will be created.

NOTE: TARDIS in this context stands for Time and Relative Dimension in Snapshots.  Any resemblence to the "Time and Relative Dimension in Space" TARDIS from BBC's Doctor Who is purely me being a huge fan.  If anybody has an issue with this, please contact me and I'll look into changing the name to something less cool.
