1. Implement global configuration file in ~/.config/tardis.conf, /etc/tardis.conf
	- Control what filesystems are allowed
	- Configure filesystem-specific options (rsnapshot/rsync config, tar config, etc)
2. Implement rsync/rsnapshot
	- remote destinations
3. Implement tar filesystem
	- remote destination
4. Implement TimeMachine
