from setuptools import setup, find_packages

setup(
    name='Tardis',
    author='Graeme Lawes',
    author_email='graemelawes@gmail.com',
    url='https://github.com/gclawes/tardis',
    version='0.4.1',
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'tardis=tardis.cli:main'
        ]
    },
    license='FreeBSD',
    requires=['sh']
)
