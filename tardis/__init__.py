from tardis.logs import logger as log
from tardis.snapshot import Snapshot
from tardis.namespace import *

__all__ = ["filesystems", "log", "env", "Snapshot"]

env = EnvNamespace({
    'DEBUG': False,
    'DRY_RUN': False
})

from tardis.fsplugins import load_plugins

filesystems = load_plugins()
