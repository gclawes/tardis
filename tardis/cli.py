"""Usage:
    tardis auto-snapshot [OPTIONS].. INTERVAL KEEP
    tardis list [OPTIONS]
    tardis config [OPTIONS]

Options:
    --filesystem    -f  Select filesystem (ZFS, btrfs, etc)
    --no-action     -n  Take no action, show what would be done
"""
import sys
import argparse
import tardis

import logging


def auto_snapshot(args):
    if args.keep == "0":
        tardis.log.error("'keep' can't be zero!")
        sys.exit(1)

    if args.filesystem is not None:
        if args.filesystem in tardis.filesystems.keys():
            filesystems = [tardis.filesystems[args.filesystem]]
        else:
            tardis.log.error("invalid filesystem '%s', valid choices: %s" %
                             (args.filesystem, ', '.join(tardis.filesytems.keys())))
            sys.exit()
    else:
        filesystems = tardis.filesystems.values()

    for fs in filesystems:
        fs.auto_snapshot(args.interval, args.keep)
        fs.purge_old_snapshots(args.interval, args.keep)
        fs.clean_zero_size_snapshots(args.interval)


def list_func(args):
    if args.filesystem is not None:
        if args.filesystem in tardis.filesystems.keys():
            filesystems = [tardis.filesystems[args.filesystem]]
        else:
            tardis.log.error("invalid filesystem '%s', valid choices: %s" %
                             (args.filesystem, ', '.join(tardis.filesytems.keys())))
            sys.exit()
    else:
        filesystems = tardis.filesystems.values()

    for fs in filesystems:
        for s in fs.get_snapshots():
            print "%s: %s" % (fs.name, s)


def config(args):
    if args.filesystem is not None:
        if args.filesystem in tardis.filesystems.keys():
            filesystems = [tardis.filesystems[args.filesystem]]
        else:
            tardis.log.error("Invalid filesystem '%s', valid choices: %s" %
                             (args.filesystem, ', '.join(tardis.filesystems.keys())))
            sys.exit()
    else:
        filesystems = tardis.filesystems.values()

    for fs in filesystems:
        c = fs.get_config()
        for k in c.keys():
            print "%s: %s" % (fs.name, k)
            for p in c[k].keys():
                print "    %s %s" % (p, c[k][p])
            print ""


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument('--no-action', '-n', action='store_true')
    parser.add_argument('--debug', action='store_true')

    subparsers = parser.add_subparsers()

    auto_snapshot_cmd = subparsers.add_parser('auto-snapshot')
    auto_snapshot_cmd.add_argument('--filesystem', '-f')
    auto_snapshot_cmd.add_argument('interval')
    auto_snapshot_cmd.add_argument('keep')
    auto_snapshot_cmd.set_defaults(func=auto_snapshot)

    list_cmd = subparsers.add_parser('list')
    list_cmd.add_argument('--filesystem', '-f')
    list_cmd.set_defaults(func=list_func)

    config_cmd = subparsers.add_parser('config')
    config_cmd.add_argument('--filesystem', '-f')
    config_cmd.set_defaults(func=config)

    args = parser.parse_args()
    if args.no_action:
        tardis.env['DRY_RUN'] = True
        tardis.log.info('Dry-run enabled, no filesystem changes will be made!')

    if args.debug:
        tardis.env['DEBUG'] = True
        tardis.log.setLevel(logging.DEBUG)
        tardis.log.debug('Debuging enabled')
        tardis.log.debug("Enabled filesystems: %s" % ', '.join(tardis.filesystems.keys()))

    args.func(args)
