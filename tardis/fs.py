import abc

from datetime import datetime

from tardis import log


class Filesystem(object):
    """Base interface class for filesystem in TARDIS"""
    __metaclass__ = abc.ABCMeta

    def __init__(self):
        self.name = self.__module__.split(".")[-1]

    @abc.abstractmethod
    def is_enabled(self):
        log.warning("Unimplemented Method: is_enabled()")
        return False

    @abc.abstractmethod
    def get_datasets(self):
        log.warning("no datasets for fake interface")

    @abc.abstractmethod
    def get_snapshots(self, filesystem=None):
        log.warning("Unimplemented Method: get_snapshots()")

    @abc.abstractmethod
    def get_config(self):
        log.warning("Unimplemented Method: get_config()")

    @abc.abstractmethod
    def commit_snapshot(self, filesystem, interval, label):
        log.warning("Unimplemented Method: commit_snapshot()")

    @abc.abstractmethod
    def destroy_snapshot(self, name):
        log.warning("Unimplemented Method: destroy_snapshot()")

    @abc.abstractmethod
    def get_snapshot_size(self, snapshot):
        log.warning("Unimplemented Method: get_snapshot_size()")

    @abc.abstractmethod
    def print_snapshot(self, dataset, label):
        log.warning("Unimplemented Method: print_snapshot()")

    def filter_snapshots(self, dataset=None, interval=None):
        snapshots = self.get_snapshots()
        retval = []
        if dataset and interval:
            for s in snapshots:
                if dataset == s.dataset and s.interval == interval:
                    retval.append(s)
        elif dataset and not interval:
            for s in snapshots:
                if s.dataset == dataset:
                    retval.append(s)
        elif interval and not dataset:
            for s in snapshots:
                if s.interval == interval:
                    retval.append(s)
        else:
            raise Exception("Invalid filter!")
        return retval

    @property
    def intervals(self):
        """get the intervals within a list of snapshots"""
        intervals = set()
        for s in self.get_snapshots():
            if s.is_auto_snap:
                if s.interval not in intervals:
                    intervals.add(s.interval)
        return list(intervals)

    def get_old_snapshots(self, keep, interval=None):
        old_snapshots = []
        for d in self.get_datasets():
            if interval:
                snapshots = self.filter_snapshots(dataset=d, interval=interval)
                if keep == 0:
                    old_snapshots = []
                else:
                    old_snapshots = old_snapshots + snapshots[:-keep]
            else:
                log.warning("get_old_snapshots: THIS SHOULD PROBABLY NEVER RUN!")
                for i in self.intervals:
                    x = [s for s in self.get_snapshots() if s.interval == i]
                    if keep == 0:
                        old_snapshots = []
                    else:
                        old_snapshots = old_snapshots + x[:-keep]
        return old_snapshots

    def purge_old_snapshots(self, interval, keep):
        old_snapshots = self.get_old_snapshots(int(keep), interval)
        if old_snapshots != []:
            log.info("purging old snapshots: %s" %
                    (' '.join([str(s) for s in old_snapshots])))
            for s in old_snapshots:
                s.destroy()

    def clean_zero_size_snapshots(self, interval_name):
        #all_snapshots = self.get_snapshots()
        for d in self.get_datasets():
            d_snapshots = self.filter_snapshots(dataset=d, interval=interval_name)[:-1]
            for s in d_snapshots:
                if s.size == "0":
                    log.info("destroying zero-sized snapshot: %s" % s)
                    s.destroy()

    def auto_snapshot(self, interval, keep):
        """create auto-snapshot set with interval,
        purging old snapshots"""
        fs_config = self.get_config()
        now = datetime.now()
        date = "%s-%s-%s-%sh%s" % (str(now.year), str(now.month).zfill(2),
                                   str(now.day).zfill(2),
                                   str(now.hour).zfill(2),
                                   str(now.minute).zfill(2))
        key = "auto-snapshot:%s" % interval

        to_snapshot = []
        exclude = []

        for dataset in sorted(fs_config.keys()):
            if fs_config[dataset]['auto-snapshot']:
                if key in fs_config[dataset].keys():
                    if fs_config[dataset][key]:
                        to_snapshot.append(dataset)
                    else:
                        exclude.append(dataset)
                else:
                    to_snapshot.append(dataset)
            else:
                exclude.append(dataset)

        log.debug("Snapshotting the following: %s" % ' '.join(to_snapshot))
        log.debug("Excluding the following: %s" % ' '.join(exclude))

        for dataset in to_snapshot:
            self.commit_snapshot(dataset, interval, date)
