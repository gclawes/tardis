# check out https://github.com/platipy/spyral/blob/master/spyral/__init__.py
# for the inspiration for this

"""imports each filesystem plugin for tardis"""

__version__ = '0.1dev'
__license__ = ""
__author__ = 'Graeme Lawes'

import os
import imp
from fnmatch import filter

from tardis import log
from tardis.fs import Filesystem

mod_files = filter(os.listdir(os.path.dirname(__file__)), "*.py")
mod_files.remove('__init__.py')
names = [f[:-3] for f in mod_files]

loaded_modules = {}
for mod_name in names:
    fd, pathname, description = imp.find_module(mod_name, __path__)
    try:
        module = imp.load_module("%s.%s" % (__name__, mod_name),
                                 fd, pathname, description)
        loaded_modules[mod_name] = module
    except:
        log.error("Error importing %s" % mod_name)
        raise
    finally:
        fd.close()

if not loaded_modules:
    raise Exception("ERROR: no filesystems enabled!")


def load_plugins():
    """Loads filesystem plugins.  Returns a list of instances"""
    return {p.__module__.split('.')[-1]: p()
            for p in Filesystem.__subclasses__() if p.is_enabled()}
