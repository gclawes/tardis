import sys
import os
import stat

from csv import reader
from StringIO import StringIO

from tardis import log, env, Snapshot
from tardis.fs import Filesystem

import re

try:
    from sh import btrfs as btrfs_cmd
    BTRFS_AVAILABLE = True
except:
    BTRFS_AVAILABLE = False
    log.debug("unable to find btrfs command")

from sh import ErrorReturnCode_19


def dir_is_writable(directory):
    s = os.stat(directory)
    mode = s[stat.ST_MODE]
    return bool(
        (mode & stat.S_IWUSR) or
        (mode & stat.S_IWGRP) or
        (mode & stat.S_IWOTH)
    )


class Btrfs(Filesystem):
    name = "btrfs"
    DEFAULT_POOL = "/mnt/btrfs"
    CONFIG_FILE = '/etc/tardis/btrfs.json'

    @classmethod
    def is_enabled(cls):
        if os.geteuid() != 0:
            log.debug("btrfs is only enabled for the root user!")
            return False

        if not sys.platform.startswith('linux'):
            log.debug("btrfs is only enabled on linux!")
            return False

        elif BTRFS_AVAILABLE:
            if "v0.19" in btrfs_cmd("--version"):
                log.warning("'btrfs' command v0.19 is incompatible, please upgrade to at least v0.20 to enable btrfs")
                return False
            return True
        else:
            return False

    def get_datasets(self):
        """
        NOTE: This will only work for systems with a single btrfs filesystem
        """
        # TODO: fix this!
        fs_tree = re.compile('^<FS_TREE>/.*')

        mount_raw = reader(open('/proc/mounts', 'r'), delimiter=' ')
        mount_points = [row[1] for row in mount_raw if row[2] == 'btrfs']

        # first case: 'default' subvolume is mounted somewhere
        # locate all subvolumes located under this mount point
        # TODO: accumulate subvolumes from multiple btrfs filesystems with
        # 'default' mounted, as well as filesystems with default unmounted
        for point in mount_points:
            raw_list = btrfs_cmd.subvolume.list("-a", "--sort=path", point).stdout
            separated_list = list(reader(StringIO(raw_list), delimiter=" "))
            subvols = [row[8] for row in separated_list
                       if ".btrfs-auto-snap" not in row[8]
                       and ".snapshots" not in row[8]]
            if not all([fs_tree.match(s) is not None for s in subvols]):
                return [os.path.join(point, s) for s in subvols]

        # case 2: 'default' subvolume isn't mounted somewhere
        # just use all available mount points
        return [p for p in mount_points if ".btrfs-auto-snap" not in p]

    def get_snapshots(self, root=DEFAULT_POOL):
        snapshots = []

        datasets = self.get_datasets()
        for dataset in datasets:
            # BTRFS in kernel has a weird bug where listing will fail
            # immediately after a filesystem update.  Loop on list until
            # it is successful
            try_again = True
            failure = False
            while try_again:
                try:
                    subvol_reader = reader(StringIO(btrfs_cmd.subvolume.list("--sort=path", "-s", dataset)), delimiter=" ")
                    if failure:
                        log.debug("btrfs listing finished")
                    try_again = False
                except ErrorReturnCode_19:
                    if not failure:
                        failure = True
                        log.debug("btrfs listing failed")
            for row in subvol_reader:
                if 'btrfs-auto-snap' in row[-1:][0] and row[-1:][0].startswith('.btrfs-auto-snap'):
                    _, interval, label = row[-1:][0].split("/")
                    snapshots.append(Snapshot(self, dataset, interval, label, size=1))

        return snapshots

    def get_config(self):
        """Get configuration from the filesystem layout.

        Looks in the root of each subvolume for a .btrfs-auto-snap directory.
        If this directory is not present, it counts the subvolume as disabled entirely.

        If the file exists, then all directories under that directory are the enabled labels.
        """

        datasets = self.get_datasets()
        config = {}

        for dataset in datasets:
            snap_dir = "%s/.btrfs-auto-snap" % dataset
            if os.path.isdir(snap_dir) and not os.path.islink(snap_dir):
                intervals = [d for d in os.listdir(snap_dir)
                             if os.path.isdir(os.path.join(snap_dir, d))]
                config[dataset] = {'auto-snapshot': dir_is_writable(snap_dir)}
                for interval in intervals:
                    if dir_is_writable(os.path.join(snap_dir, interval)):
                        config[dataset]['auto-snapshot:%s' % interval] = True
                    else:
                        config[dataset]['auto-snapshot:%s' % interval] = False
            else:
                config[dataset] = {'auto-snapshot': False}
        return config

    def commit_snapshot(self, dataset, interval, label):
        """call out to btrfs command to create snapshot
        return a Snapshot object"""

        snap_dir = os.path.join(dataset, '.btrfs-auto-snap')
        interval_dir = os.path.join(snap_dir, interval)
        destination = os.path.join(interval_dir, label)

        if os.path.exists(destination):
            log.error("snapshot %s already exists, not overwriting!" % destination)
            return

        # if interval_dir isn't a directory, and also isn't a file with that name
        # create the snapshot label directory
        if not os.path.isdir(interval_dir) and not os.path.exists(interval_dir):
            s = os.stat(snap_dir)
            os.mkdir(interval_dir)
            os.chown(interval_dir, s[stat.ST_UID], s[stat.ST_GID])

        if env.DRY_RUN:
            log.info("dry-run: would commit %s" % (destination))
        else:
            log.debug("commit_snapshot(%s, %s, %s)" % (dataset, interval, label))
            btrfs_cmd.subvolume.snapshot(dataset, destination)

    def destroy_snapshot(self, name):
        """callout to btrfs command to destroy snapshot"""

        if env.DRY_RUN:
            log.info("dry-run: would destroy %s" % name)
        else:
            log.debug("destroy_snapshot(%s)" % (name))
            if os.access(name, os.F_OK):
                if "/.btrfs-auto-snap/" in name:
                    btrfs_cmd.subvolume.delete(name)
                else:
                    log.error("should not be destroying a dataset! name: %s" % name)
            else:
                log.error("cannot find %s" % name)

    def get_snapshot_size(self, snapshot):
        """BTRFS does not have an easy way to track snapshot size,
        always assume a snapshot has nonzero size"""
        return 1

    def print_snapshot(self, dataset, interval, label):
        return os.path.join(dataset, '.btrfs-auto-snap', interval, label)
