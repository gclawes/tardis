import re

from csv import reader
from StringIO import StringIO

from tardis import log, env, Snapshot
from tardis.fs import Filesystem

try:
    from sh import zfs as zfs_cmd
    ENABLED = True
except:
    ENABLED = False


class Zfs(Filesystem):
    @classmethod
    def is_enabled(cls):
        return ENABLED

    def get_datasets(self):
        raw_list = zfs_cmd.list("-H")
        l = reader(StringIO(raw_list), delimiter="\t")
        return [row[0] for row in l]

    def get_snapshots(self, filesystem=None):
        """retrieve all ZFS snapshots marked as auto-snapshot"""

        if filesystem is None:
            raw_list = zfs_cmd.list("-H", "-t", "snapshot")
        else:
            raw_list = zfs_cmd.list("-H", "-r", "-d1", "-t", "snapshot", filesystem)
        zfs_reader = reader(StringIO(raw_list), delimiter="\t")
        snapshots = []
        for row in zfs_reader:
            if "zfs-auto-snap" in row[0]:
                dataset, name = row[0].split("@")
                _, temp = name.split("_")
                split = temp.split("-")
                interval = split[0]
                label = "-".join(split[1:])
                snapshots.append(Snapshot(self, dataset, interval, label, size=row[1]))
        return snapshots

    def get_config(self):
        """load com.sun:auto-snapshot settings
        for all zfs filesystems"""
        raw_config = zfs_cmd.get("-o", "name,property,value", "-t", "filesystem", "all")

        p = re.compile('.* com.sun:auto-snapshot.*')
        subst = re.compile("com.sun:")

        config = {}

        for i in p.findall(raw_config.stdout):
            s = i.split()
            if len(s) == 3:
                name = s[0]
                prop = subst.sub("", s[1])
                if s[2] == "true":
                    value = True
                elif s[2] == "false":
                    value = False
                else:
                    log.error("improper value for com.sun:auto-snapshot property, assuming false: %s %s %s" % (name, prop, value))
                    value = False

                if name in config.keys():
                    config[name][prop] = value
                else:
                    config[name] = {prop: value}

        return config

    def commit_snapshot(self, filesystem, interval, label):
        """call out to zfs command to create snapshot
        return a Snapshot object"""

        name = "%s@zfs-auto-snap_%s-%s" % (filesystem, interval, label)
        current_snapshots = [s.name for s in self.get_snapshots()]

        if name in current_snapshots:
            log.error("snapshot %s already exists, not overwriting!" % name)
            return

        if env.DRY_RUN:
            log.info("dry-run: would commit %s" % name)
        else:
            log.debug("commit_snapshot(%s, %s, %s)" % (filesystem, interval, label))
            zfs_cmd.snapshot(name)

    def destroy_snapshot(self, name):
        """callout to zfs command to destroy snapshot"""
        #name = "%s@%s" % (filesystem, interval)

        if env.DRY_RUN:
            log.info("dry-run: would destroy %s" % name)
        else:
            log.debug("destroy_snapshot(%s)" % (name))
            if "@" in name:
                zfs_cmd.destroy(name)
            else:
                log.error("should not be destroying a dataset! name: %s" % name)

    def get_snapshot_size(self, snapshot):
        cmd_out = zfs_cmd.list("-H", snapshot.name)
        size = cmd_out.split("\t")[1]
        if size == '0':
            return 0
        else:
            return size

    def print_snapshot(self, dataset, interval, label):
        return "%s@zfs-auto-snap_%s-%s" % (dataset, interval, label)
