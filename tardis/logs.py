import logging
from logging import handlers
from socket import AF_UNIX


logger = logging.getLogger('tardis')
log_format = logging.Formatter(fmt='tardis: %(message)s')
logger.setLevel(logging.WARNING)

stream_handler = logging.StreamHandler()
stream_handler.setFormatter(log_format)
logger.addHandler(stream_handler)

try:
    syslog_handler = handlers.SysLogHandler(address='/dev/log', socktype=AF_UNIX)
except:
    syslog_handler = handlers.SysLogHandler(address='/dev/log')
finally:
    try:
        syslog_handler.setFormatter(log_format)
        logger.addHandler(syslog_handler)
    except:
        pass
