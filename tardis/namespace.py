"""namespace module

Modified namespace class from: http://code.activestate.com/recipes/577887-a-simple-namespace-class/

Acts as a dictionary that can be overridden by environment variables with a certain prefix.
"""

__all__ = ["EnvNamespace", "env_namespace"]

import os
import re
from collections import Mapping, Sequence


class _Dummy(object):
    pass
CLASS_ATTRS = dir(_Dummy)
del _Dummy

# match a PATH like list from the environment
path_list_re = re.compile(r':?\S+(:\S+)+:?')
csv_list_re = re.compile(r':?\S+(:\S+)+:?')


class EnvNamespace(dict):
    """A dict subclass that exposes its items as attributes.

    Warning: Namespace instances do not have direct access to the
    dict methods.

    """
    def __init__(self, obj={}):
        super(EnvNamespace, self).__init__(obj)

    def __dir__(self):
        return list(self)

    def __repr__(self):
        return "%s(%s)" % (type(self).__name__, super(type(self), self).__repr__())

    #def __getattribute__(self, name):
    #    try:
    #        return self[name]
    #    except KeyError:
    #        msg = "'%s' object has no attribute '%s'"
    #        raise AttributeError(msg % (type(self).__name__, name))

    def __getattribute__(self, name):
        env_name = "TARDIS_%s" % name
        if env_name in os.environ:
                value = os.environ[env_name]
        elif env_name.lower() in os.environ:
                value = os.environ[env_name.lower()]
        elif env_name.upper() in os.environ:
                value = os.environ[env_name.upper()]
        else:
            try:
                # is this instance defined as a @property
                if isinstance(self[name], property):
                    value = self[name].fget(self)
                else:
                    value = self[name]
            except KeyError:
                msg = "'%s' object has no attribute '%s'"
                raise AttributeError(msg % (type(self).__name__, name))
        if path_list_re.match(str(value)):
            value = tuple(value.split(':'))
        elif csv_list_re.match(str(value)):
            value = tuple(value.split(','))

        return value

    def __setattr__(self, name, value):
        if isinstance(self[name], property):
            if self[name].fset is None:
                raise AttributeError("can't set dynamic attribute %s" % name)
        self[name] = value

    def __delattr__(self, name):
        del self[name]

    #------------------------
    # "copy constructors"

    @classmethod
    def from_object(cls, obj, names=None):
        if names is None:
            names = dir(obj)
        ns = {name: getattr(obj, name) for name in names}
        return cls(ns)

    @classmethod
    def from_mapping(cls, ns, names=None):
        if names:
            ns = {name: ns[name] for name in names}
        return cls(ns)

    @classmethod
    def from_sequence(cls, seq, names=None):
        if names:
            seq = {name: val for name, val in seq if name in names}
        return cls(seq)

    #------------------------
    # static methods

    @staticmethod
    def hasattr(ns, name):
        try:
            object.__getattribute__(ns, name)
        except AttributeError:
            return False
        return True

    @staticmethod
    def getattr(ns, name):
        return object.__getattribute__(ns, name)

    @staticmethod
    def setattr(ns, name, value):
        return object.__setattr__(ns, name, value)

    @staticmethod
    def delattr(ns, name):
        return object.__delattr__(ns, name)


def env_namespace(obj, names=None):

    # functions
    if isinstance(obj, type(env_namespace)):
        obj = obj()

    # special cases
    if isinstance(obj, type):
        names = (name for name in dir(obj) if name not in CLASS_ATTRS)
        return EnvNamespace.from_object(obj, names)
    if isinstance(obj, Mapping):
        return EnvNamespace.from_mapping(obj, names)
    if isinstance(obj, Sequence):
        return EnvNamespace.from_sequence(obj, names)

    # default
    return EnvNamespace.from_object(obj, names)
