import re

from datetime import datetime

date_re = re.compile('^(?P<year>[0-9]{4})-(?P<month>(0[0-9]|1[0-2]))-(?P<day>([0-2][0-9]|3[01]))-(?P<hour>([01][0-9]|2[0-4]))h(?P<minute>([0-5][0-9]|60))')


class Snapshot(object):
    def __init__(self, module, dataset, interval, label, size=None):
        """using ZFS cannonical name for snapshot"""

        self.module = module
        self.dataset = dataset
        self.interval = interval
        self.label = label
        self.prefix = "%s-auto-snap" % self.module.name

        match = date_re.match(self.label)
        if match:
            self.is_auto_snap = True
            date = {key: int(value) for key, value in match.groupdict().items()}
            self.datetime = datetime(date['year'], date['month'], date['day'], date['hour'], date['minute'])
        else:
            self.is_auto_snap = False
            self.datetime = None

        if size is not None:
            self.size = size
        else:
            self.size = self.module.get_snapshot_size(self)

    def __repr__(self):
        return "Snapshot(%s, %s, %s)" % (self.dataset, self.interval, self.label)

    def __str__(self):
        return self.name

    @property
    def name(self):
        return self.module.print_snapshot(self.dataset, self.interval, self.label)

    def destroy(self):
        self.module.destroy_snapshot(self.name)
