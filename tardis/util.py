from types import ModuleType


class IterableModule(ModuleType):
    def __init__(self, name, mod_dict):
        super(IterableModule, self).__init__(name, mod_dict.values())
        #ModuleType.__init__(self, name)
        #list.__init__(self, mod_dict.value())
        self.mod_dict = mod_dict
        self.keys = iter(self.mod_dict.keys())
        self.current = self.keys.next()

    def __getattr__(self, name):
        if name in self.mod_dict.keys():
            setattr(self, name, self.mod_dict[name])
            return getattr(self, name)

    def __dir__(self):
        result = self.mod_dict.keys()
        result.extend(('__file__', '__path__', '__doc__', '__all__',
                       '__docformat__', '__name__', '__path__',
                       '__package__', '__version__'))
        return result

    def __iter__(self):
        return iter(self.mod_dict.values())

    def next(self):
        retval = self.current
        self.keys = self.keys.next()
        return retval
